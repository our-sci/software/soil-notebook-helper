const LS_API_KEY = 'API_KEY';

let fields = [];
let areas = [];
let areaIdForSelectedField = '';
let currentFieldId = '';

// Environment Elements
const environmentSelect = document.querySelector('#environmentSelect');
// Credentials Elements
const credentialsInput = document.querySelector('#credentialsInput');
const credentialsInputHideShowButton = document.querySelector('#credentialsInputHideShowButton');
// Get Fields Elements
const getFieldsButton = document.querySelector('#getFieldsButton');
const fieldsSearchInput = document.querySelector('#fieldsSearchInput');
const fieldsSelect = document.querySelector('#fieldsSelect');
const fieldPre = document.querySelector('#fieldPre');
// Post Stratification Elements
const submitStratificationButton = document.querySelector('#submitStratificationButton');
const submitStratificationStatus = document.querySelector('#submitStratificationStatus');
const stratificationPre = document.querySelector('#stratificationPre');
// CSV Elements
const locationsCSVPre = document.querySelector('#locationsCSVPre');

// Event Handlers
const handleGetFields = () => {
  getFieldsStatus.innerText = `Status: Fetching Fields...`;
  const { SOIL_API_URL } = getEnvironmentVars();
  fetch(`${SOIL_API_URL}/resources`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `apikey ${localStorage.getItem(LS_API_KEY)}`,
    },
  })
  .then(async resp => { 
    if (resp.status === 200) {
      const data = await resp.json();
      getFieldsStatus.innerText = `Status: Fields List Populated`;
      fields = data.fields;
      areas = data.areas;
      populateFieldsSelect();
    } else if (resp.status === 401) {
      fields = [];
      areas = [];
      getFieldsStatus.innerText = `Status: ERROR:: Unauthorized. Confirm you have the correct API key, and that the selected environment is where your API key is from.`;
    }
  })
  .catch(error => {
    console.log({ error })
    fields = [];
    areas = [];
    getFieldsStatus.innerText = `Status: ERROR:: ${error}`;
  });
};

const handleFieldSelect = () => {
  const field = fields.find(field => field.id === fieldsSelect.value);
  currentFieldId = field.id;
  const area = areas.find(area => area.id === field.areas[0]);
  areaIdForSelectedField = area.id;
  fieldPre.innerText = JSON.stringify({
    fieldId: field.id,
    fieldName: field.name,
    area,
    referenceIds: field?.meta?.referenceIds ?? [],
  });
  stratificationPre.innerText = '';
  locationsCSVPre.innerText = '';
};

const handleSubmitStratification = () => {
  const { SOIL_API_URL } = getEnvironmentVars();
  const stratificationRequestBody = JSON.parse(stratificationPre.innerText);
  submitStratificationStatus.innerText = 'Status: Submitting...';
  // Remove properties that can no longer be passed as of https://gitlab.com/our-sci/software/soil-api/-/wikis/Breaking-Changes-April-17th,-2023
  // This allows older version of the notebook to still be used with this helper.
  delete stratificationRequestBody?.locationCollection?.resultOf;
  delete stratificationRequestBody?.locationCollection?.featureOfInterest;
  delete stratificationRequestBody?.locationCollection?.object;
  delete stratificationRequestBody?.stratification?.object;
  fetch(`${SOIL_API_URL}/areas/${areaIdForSelectedField}/stratifications`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `apikey ${localStorage.getItem(LS_API_KEY)}`,
    },
    body: JSON.stringify(stratificationRequestBody)
  })
  .then(async resp => {
    if (resp.status === 201) {
      const data = await resp.json();
      submitStratificationStatus.innerText = 'Status: Stratification Submitted Successfully';
      populateCSV(data);
    } else if (resp.status === 401 || resp.status === 404) {
      submitStratificationStatus.innerText = `Status: ERROR:: Unauthorized or Not Found. Confirm you have the correct API key, and that the selected environment is where your API key is from. If those are correct, the user your api key belongs to may not have access to post a stratification to the group the field is in.`;
    } else {
      const data = await resp.json();
      submitStratificationStatus.innerText = `Status: ERROR:: ${data.message}`;
    }
  })
  .catch(error => {
    submitStratificationStatus.innerText = `Status: ERROR:: ${error}`;
  });
}

// Utils
const getEnvironmentVars = () => {
  switch(environmentSelect.value) {
    case 'prod':
      return {
        SOIL_API_URL: 'https://api.soilstack.io',
      };
    case 'stage':
      return {
        SOIL_API_URL: 'https://stage.api.soilstack.io',
      };
    default:
      alert('Error: No Environment selected somehow.');
  }
}

const createOption = (value, innerText) => {
  const option = document.createElement('option');
  option.setAttribute('value', value);
  option.innerText = innerText;
  return option;
};

const populateCSV = data => {
  const headers = ['fieldId', 'stratificationId', 'locationCollectionId', 'locationId', 'x', 'y', 'stratum'];
  const rows = data.locations.map(location => [
    currentFieldId,
    data.stratifications[0].id,
    data.locationCollections[0].id,
    location.id,
    location.geometry.coordinates[0],
    location.geometry.coordinates[1],
    location.properties.stratum,
  ]);
  const csv = [headers, ...rows].map(row => row.join(',')).join('\n');
  locationsCSVPre.innerText = csv;
}

const populateFieldsSelect = () => {
  const filterQuery = fieldsSearchInput.value;
  fieldsSelect.innerHTML = '';
  fieldsSelect.size = Math.min(15, fields.length);
  fields.forEach(field => {
    if (
      field.name.toLowerCase().includes(filterQuery.toLowerCase())
      || field.id.toLowerCase().includes(filterQuery.toLowerCase())
    ) {
      fieldsSelect.appendChild(createOption(field.id, `${field.name} (${field.id})`))
    }
  });
}

// Hydrate from localStorage
credentialsInput.value = localStorage.getItem(LS_API_KEY);

// Event Listeners
credentialsInput.addEventListener('change', (event) => {
  localStorage.setItem(LS_API_KEY, event.target.value);
});
credentialsInputHideShowButton.addEventListener('click', () => {
  if (credentialsInput.type === 'password') {
    credentialsInput.type = 'text';
  } else {
    credentialsInput.type = 'password';
  }
});
getFieldsButton.addEventListener('click', handleGetFields);
fieldsSelect.addEventListener('change', handleFieldSelect);
fieldsSearchInput.addEventListener('input', populateFieldsSelect);
submitStratificationButton.addEventListener('click', handleSubmitStratification);